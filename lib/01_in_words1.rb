class Fixnum
  
  def in_words
    self.eval
  end
  
  def eval
    array = []
    num = self.to_s.chars.reverse
    places = [4,7,10,13]
    length = 0
    num.each_with_index do |int, index|
      length += 1
      array << length.add_place if places.include?(length)
      
      if index % 3 == 0
        array << int.to_i.ones
      elsif index % 3 == 1
        if int.to_i == 1
          array.pop
          array << num[index-1].to_i.teens
        elsif array[-1] == "zero"
          array.pop
          array << int.to_i.tens
        else
          array << int.to_i.tens
        end
      elsif int.to_i > 0
        array << "hundred"
        array << int.to_i.ones
      end
    end
    array.reverse.join(" ")
  end
  
  def add_place
    case self
      when 4
        "thousand"
      when 7
        "million"
      when 10
        "billion"
      when 13
        "trillion"
      end
  end
  
  def ones
    case self
      when 0
        "zero"
      when 1
        "one"
      when 2
        "two"
      when 3
        "three"
      when 4
        "four"
      when 5
        "five"
      when 6
        "six"
      when 7
        "seven"
      when 8
        "eight"
      when 9
        "nine"
    end
  end
  
  def tens
    case self
      when 2
        "twenty"
      when 3
        "thirty"
      when 4
        "forty"
      when 5
        "fifty"
      when 6
        "sixty"
      when 7
        "seventy"
      when 8
        "eighty"
      when 9
        "ninety"
    end
  end
  
  def teens
    case self
      when 0
        "ten"
      when 1
        "eleven"
      when 2
        "twelve"
      when 3
        "thirteen"
      when 4
        "fourteen"
      when 5
        "fifteen"
      when 6
        "sixteen"
      when 7
        "seventeen"
      when 8
        "eighteen"
      when 9
        "nineteen"
    end
  end
  

end
