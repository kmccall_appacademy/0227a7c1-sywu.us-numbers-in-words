class Fixnum
  
  def in_words
    return "zero" if self == 0
    array = []
    num = self
    while num > 0
      length = num.get_mod
      array << (num/length).eval
      if length > 1
        array << length.get_length
      end
      num %= length
    end
    array.join(" ")
  end
  
  def eval
    array = []
    
    if self > 99
      array << (self/100).ones
      array << "hundred"
      if self % 100 == 0
        return array.join(" ")
      else
        array << (self%100).eval
      end
    elsif self > 19
      array << (self/10).tens
      if self % 10 == 0
        return array.join(" ")
      else
        array << (self%10).eval
      end
    elsif self > 9
      array << self.teens
      return array.join(" ")
    else
      array << self.ones
    end
    array.join(" ")
  end
  
  def get_mod
    if self >= 1000000000000
      1000000000000
    elsif self >= 1000000000
      1000000000
    elsif self >= 1000000
      1000000
    elsif self >= 1000
      1000
    else
      1
    end
  end
  
  def get_length
    case self
    when 1000
      "thousand"
    when 1000000
      "million"
    when 1000000000
      "billion"
    when 1000000000000
      "trillion"
    end
  end
  
  def ones
    case self
      when 0
        "zero"
      when 1
        "one"
      when 2
        "two"
      when 3
        "three"
      when 4
        "four"
      when 5
        "five"
      when 6
        "six"
      when 7
        "seven"
      when 8
        "eight"
      when 9
        "nine"
    end
  end
  
  def tens
    case self
      when 2
        "twenty"
      when 3
        "thirty"
      when 4
        "forty"
      when 5
        "fifty"
      when 6
        "sixty"
      when 7
        "seventy"
      when 8
        "eighty"
      when 9
        "ninety"
    end
  end
  
  def teens
    case self
    when 10
        "ten"
      when 11
        "eleven"
      when 12
        "twelve"
      when 13
        "thirteen"
      when 14
        "fourteen"
      when 15
        "fifteen"
      when 16
        "sixteen"
      when 17
        "seventeen"
      when 18
        "eighteen"
      when 19
        "nineteen"
    end
  end
  
end
